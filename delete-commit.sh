#!/bin/bash

# Check if a commit hash is provided as an argument
if [ -z "$1" ]; then
  echo "Usage: $0 <commit_hash>"
  exit 1
fi

# Store the commit hash provided as an argument
commit_hash=$1

# Perform git rebase to move the branch pointer of main
sudo git rebase --onto "$commit_hash" main

# Perform git reset to delete the latest commit on the main branch
sudo git reset --hard HEAD~1


echo "Commit with hash $commit_hash deleted from main branch and changes pushed to remote."

